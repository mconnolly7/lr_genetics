fid = fopen('allele_counts_transposed.txt');

num_line    = nan(500000, 194);
next_line   = fgetl(fid);
c1          = 1;
while ~isempty(next_line)
    
    split_line      = split(next_line, ' ');
    mat_line        = cell2mat(split_line);
    num_line(c1,:)  = str2num(mat_line);
    
    next_line       = fgetl(fid);
    c1              = c1 + 1;
    
    if mod(c1, 1000) == 0
        fprintf('Line %d\n', c1)
    end
end

fclose(fid)
