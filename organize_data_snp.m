clc; clear;

pca_table   = readtable('raw_data/PCA_1-2_features.csv');
demo_table  = readtable('raw_data/demographic_features.csv');
score_table = readtable('raw_data/score_features.txt');

pca_table.Properties.VariableNames{1} = 'ID';


data_table  = innerjoin(demo_table,pca_table,'LeftKeys', 1, 'RightKeys', 1);
data_table  = innerjoin(data_table,score_table,'LeftKeys', 1, 'RightKeys', 1);

labels_strings          = data_table.Status;

data_table.GRS_UNWTD    = [];
data_table.GRS_WTD      = [];
data_table.IBD          = [];
data_table.ID           = [];
data_table.Status       = [];
data_table.Gender       = [];
data_table.Age          = [];
data_table.BRITISH      = [];

data_table.PC1          = [];
data_table.PC2          = [];

% Encode ethnicities
eth_strs    = unique(data_table.Ethnicity);
for c1 = 1:size(eth_strs,1)
   idx              = strcmp(data_table.Ethnicity,eth_strs{c1});
   eth_code(idx,1)  = c1;
end
data_table.Ethnicity = eth_code;

% Encode genders
sex_strs    = unique(data_table.Sex);
for c1 = 1:size(sex_strs,1)
   idx                  = strcmp(data_table.Sex,sex_strs{c1});
   gender_code(idx,1)   = c1;
end
data_table.Sex = gender_code;

% Encode labels
case_idx            = strcmp(labels_strings, 'case');
control_idx         = strcmp(labels_strings, 'control');

labels(case_idx)    = 1;
labels(control_idx) = 0;

save('processed_data/clean_data.mat', 'labels', 'data_table');