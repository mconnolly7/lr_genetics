%%
clear; close all; clc
% rng(0);

load('processed_data/clean_data')

%% set up training and validation sets
k_outer     = 30;
k_inner     = 10;

X_all           = data_table.Variables;
Y_all           = labels';
cv_idx_outer    = crossvalind('kfold', Y_all, k_outer);

for c2 = 1:k_outer
    fprintf('Outer fold: %d\n', c2);
    X_cv        = X_all(cv_idx_outer ~= c2,:);
    Y_cv        = Y_all(cv_idx_outer ~= c2,:);
    
    % Fold over the prevalence vs. percentile calculation
    X_val       = X_all(cv_idx_outer == c2,:);
    Y_val       = Y_all(cv_idx_outer == c2,:);

    % Use the cross-validation set to create balanced data
    X_control   = X_cv(Y_cv==0,:);
    X_case      = X_cv(Y_cv==1,:);

    Y_control   = Y_cv(Y_cv==0);
    Y_case      = Y_cv(Y_cv==1);

    perm_idx    = randperm(size(X_control,1));
    perm_idx    = perm_idx(1:size(X_case,1));

    X           = [X_case; X_control(perm_idx,:)];
    Y           = [Y_case; Y_control(perm_idx,:)];

    clear X_train Y_train

    % Set up the folds
    [N, M]          = size(X);
    cv_idx_inner    = crossvalind('kfold', Y, k_inner);

    %% Cross validate with training set
    for c1 = 1:max(cv_idx_inner)
        fprintf('\tInner fold: %d\n', c1);

        X_train = X(cv_idx_inner ~= c1,:);
        Y_train = Y(cv_idx_inner ~= c1,:);

        X_test  = X(cv_idx_inner == c1,:);
        Y_test  = Y(cv_idx_inner == c1,:);

        scores_tru{c1}      = Y_test;

        b = glmfit(X_train, Y_train, 'Binomial');
        scores_est{c1} = glmval(b, X_test,'probit');

    end

    % Refit with all training data
    b = glmfit(X, Y, 'Binomial');

    %% Calculate AUC
%     [x,y,~,AUC] = perfcurve(scores_tru,scores_est,1);
%     xx = [x(:,1); flip(x(:,1))];
%     yy = [y(:,2); flip(y(:,3))];

    %% Plot ROC
    % subplot(1,2,1)
    % patch(xx,yy,[.5 .5 .5], 'EdgeColor', 'none');
    % hold on
    % plot(x(:,1), y(:,1), 'k-', 'LineWidth', 2)
    % xlim([0 1])
    % ylim([0 1])
    % plot([0 1], [0 1], 'k--', 'LineWidth', 2)
    % xlabel('False Positive Rate')
    % ylabel('True Positive Rate')
    % set(gca,'FontSize', 16)
    % 
    % auc_string = sprintf('AUC: %.2f � %.2f', AUC(1), AUC(1)-AUC(2));
    % legend({'95% CI', auc_string,'Chance'})

    %% Calculates percentile vs. prevalence
    N_val                           = size(X_val,1);
    Y_val_est                       = glmval(b, X_val,'probit');

    [y_est_sort, y_est_sort_idx]    = sort(Y_val_est);

    Y_sort                          = Y_val(y_est_sort_idx);
    pctl_idx                        = floor(linspace(1, size(Y_val_est,1)));

    for c1 = 1:99
        cluster_p        = Y_sort(pctl_idx(c1):pctl_idx(c1+1));
        prevalence(c2,c1)   = sum(Y_sort(pctl_idx(c1):pctl_idx(c1+1)))/ (N_val/100)*100;
    end
end

m_prevalence    = mean(prevalence);
s_prevalence    = std(prevalence);
se_prevalence   = s_prevalence / sqrt(k_outer);
ci_prevalence   = se_prevalence*1.96;

x               = 1:99;
xx              = [x;x];
yy              = [m_prevalence - ci_prevalence ;m_prevalence + ci_prevalence];

hold on
plot(xx, yy, 'k', 'LineWidth', 2)
plot(x, m_prevalence, 'ko', 'MarkerFaceColor', [.5 .5 .5] )

xlabel('Percentile')
ylabel('Prevalence')
set(gca,'FontSize', 16)

%% Test new score
hold on
new_input       = [4 2 -6.0404 621.783 -1533.79];
new_Y           = glmval(b, new_input,'probit');

percentiles     = prctile(Y_val_est, 1:100);
[~,perc_idx]    = min(abs(percentiles - new_Y));
new_Y_perc      = percentiles(perc_idx);
new_Y_prev      = prevalence(perc_idx);

plot([perc_idx; perc_idx], [yy(1, perc_idx) yy(2, perc_idx)],'r-', 'LineWidth', 2)
plot(perc_idx, m_prevalence(perc_idx),'ro', 'MarkerFaceColor', [1 0 0 ] )
 
